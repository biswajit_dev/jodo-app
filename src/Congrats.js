/** @format */

import React from 'react';
import PropTypes from 'prop-types';

const Congrats = ({ success }) => {
  return (
    <div data-test='congrats-component'>
      {success && (
        <div data-test='congrats-message'>
          Congratulation you guess the word!
        </div>
      )}
    </div>
  );
};

Congrats.propTypes = {
    success: PropTypes.bool
}

export default Congrats;
