/** @format */

import { shallow } from 'enzyme';

import Congrats from '../Congrats';
import { findByTestAttr, checkProps } from '../../test/testConfig.utils';

const setUp = (props = {}) => {
  return shallow(<Congrats {...props} />);
};


test('render without error', () => {
  const wrapper = setUp();
  const component = findByTestAttr(wrapper, 'congrats-component');
  expect(component.length).toBe(1);
});

test('render null when `success` props is false', () => {
  const wrapper = setUp({ success: false });
  const component = findByTestAttr(wrapper, 'congrats-component');
  expect(component.text()).toBe('');
});

test('render success message when `success` props is true', () => {
  const wrapper = setUp({ success: true });
  const message = findByTestAttr(wrapper, 'congrats-message');
  expect(message.text().length).not.toBe(0);
});

test('does not throw warning with expected props', ()=> {
    const expectedProps = { success: false };
    checkProps(Congrats, expectedProps);
})