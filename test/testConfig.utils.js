/** @format */

import checkPropTypes from 'check-prop-types';

export const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test='${val}']`);
};

export const checkProps = (Component, confirmingProps) => {
  const propsError = checkPropTypes(
    Component.propTypes,
    confirmingProps,
    'prop',
    Component.name
  );
  expect(propsError).toBeUndefined();
};
